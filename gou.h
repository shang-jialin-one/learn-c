#ifndef GOU_H
#define GOU_H
#include<iostream>
#include<string>
#include<mao.h>
class gou {
public:
    gou();
    gou(std::string name);
    std::string getName();
private:
    std::string name;
    friend std::string operator+(gou g1,gou g2);
    friend std::string operator+(mao m1,gou g1);
};

#endif // GOU_H
