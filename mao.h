#ifndef MAO_H
#define MAO_H
#include<iostream>
#include<string>
#include<gou.h>
class mao {
public:
    mao();
    mao(std::string name);
    std::string getName();
private:
    std::string name;
    friend std::string operator+(mao m1,mao m2);
    friend std::string operator+(mao m1,gou g1);
};

#endif // MAO_H
